window.addEventListener('DOMContentLoaded', event => {
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});




  

  var options = {
    chart: {
      type: 'bar'
    },
    series: [{
        name: 'GCC Visitor',
        type: 'column',
        data: [140000, 200000, 250000, 150000, 250000, 280000, 300000, 206000 ,101000, 209000, 305000, 280005]
      }, {
        name: 'Others',
        type: 'column',
        data: [11000, 30000, 30000, 40000, 41000, 90000, 65000, 85000 ,41000, 49000, 60000, 80005]
      }],
    xaxis: {
      categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    },
    
      dataLabels: {
        enabled: false,
    },
    colors:['#CDA579', '#EFDAC4']
  }
  var chart = new ApexCharts(document.querySelector("#chart"), options);

  chart.render();

  var options = {
    chart: {
      type: 'bar'
    },
    series: [{
        name: 'GCC Visitor',
        type: 'column',
        data: [140000, 200000, 250000, 150000]
      }, {
        name: 'Others',
        type: 'column',
        data: [11000, 30000, 30000, 40000]
      }],
    xaxis: {
      categories: ['Etislat','Virgin Mobile','Du','Verizon'],
    },
   
      dataLabels: {
        enabled: false,
    },
    colors:['#CDA579', '#EFDAC4']
  }
  var chart = new ApexCharts(document.querySelector("#chart2"), options);

  chart.render();

  
  var donotOptions = {
          
    // series: [44, 55],
    series: [ 321567, 321567],
    labels: ['GCC Resident', 'Others'],
      chart: {
        type: 'donut',
      },
      dataLabels: {
        enabled: false,
    },
      plotOptions: {
         pie: {
          startAngle: -90,
          endAngle: 90,
          offsetY: 10,
        } 
      },
      grid: {
        padding: {
          bottom: -80
        }
      },
      colors:['#CDA579', '#EFDAC4'],
      responsive: [{
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: 'bottom'
          }
        
        }
      }]
  
  
  };
  
  var chartDonot = new ApexCharts(document.querySelector("#donot"), donotOptions);

  
  chartDonot.render();

